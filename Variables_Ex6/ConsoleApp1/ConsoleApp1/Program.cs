﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            String Nom, Prenom;
            Console.WriteLine("entrez votre nom");
            Nom = Console.ReadLine();
            Console.WriteLine("entrez votre prenom");
            Prenom = Console.ReadLine();
            Console.WriteLine("entrez votre année de naissance");
            // calcule de l'age par l'année de naissance
            int année =int.Parse(Console.ReadLine());
            int age = DateTime.Now.Year - année;

            Console.WriteLine("\n - " + Nom + "\n - " + Prenom + "\n - " + age);
        }
    }
}
