﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variable_EX5
{
    class Program
    {
        static void Main(string[] args)
        {
            String a, b;
            DateTime dateTime = DateTime.UtcNow.Date;   // DateTime.Now.ToLongDateString()  
                                                        // DateTime.Now.ToShortDateString()
            Console.WriteLine("Votre Nom :");
            a = Console.ReadLine();
            Console.WriteLine("Votre Prénom :");
            b = Console.ReadLine();
            Console.WriteLine("Bonjour "+a+" "+b+", nous sommes le "+dateTime.ToString("dd/MM/yyyy")); // pour les exemples on peut retirer dateTime.ToString()
        }
    }
}
